'use strict';

angular.module('careApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate'
])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/welcome.html',
        controller: 'loginCtrl'
      })
      .when('/1', {
        templateUrl: 'views/slider.html',
        controller: 'sliderCtrl'
      })
      .when('/1a', {
        templateUrl: 'views/1a.html',
        controller: 'sliderTextCtrl'
      })
      .when('/1b', {
        templateUrl: 'views/1b.html',
        controller: 'sliderTextCtrl'
      })
      .when('/2', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/2a', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/3', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/3a', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/4', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/5', {
        templateUrl: 'views/multi.html',
        controller: 'multiCtrl'
      })
      .when('/5a', {
        templateUrl: 'views/5a.html',
      })
      .when('/5b', {
        templateUrl: 'views/5b.html',
      })
      .when('/6', {
        templateUrl: 'views/solution.html',
        controller: 'solutionsCtrl'
      })
      .when('/7', {
        templateUrl: 'views/solution-response.html',
        controller: 'solutionsResponseCtrl'
      })
      .when('/8a', {
        templateUrl: 'views/8a.html',
        controller: 'summaryCtrl'
      })
      .when('/8c', {
        templateUrl: 'views/8c.html',
        controller: 'textCtrl'
      })
      .when('/9', {
        templateUrl: 'views/9.html',
        controller: 'textCtrl'
      })
      .when('/10', {
        templateUrl: 'views/text-response.html',
        controller: 'textCtrl'
      })
      .when('/11', {
        templateUrl: 'views/11.html',
      })
      .when('/manage', {
        templateUrl: 'views/manage.html',
        controller: 'manageCtrl'
      })
      .when('/locations', {
        templateUrl: 'views/locations.html',
        controller: 'locationsCtrl'
      })
      .when('/manage/id/:id', {
        templateUrl: 'views/donordetail.html',
        controller: 'donorDetailCtrl'
      })
      .when('/manage-login', {
        templateUrl: 'views/manage-login.html',
        controller: 'manageLoginCtrl'
      })
      .when('/manage', {
        templateUrl: 'views/manage.html',
        controller: 'manageCtrl'
      })
      .when('/manage/id/:id', {
        templateUrl: 'views/donordetail.html',
        controller: 'donorDetailCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

  });
